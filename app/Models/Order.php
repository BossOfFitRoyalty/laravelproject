<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    use HasFactory;
    /*
     * Una comanda la fa un client.
    Una comanda inclou un o més productes.
    Una comanda té unes dades generals com la data,
    i unes dades que depenen de cada producte comprat,
     com quin producte i quina quantitat se n'ha comprat,
     i a quin preu. Crea les migracions necessàries.
     */
    protected $table = 'orders';
    protected $fillable = [
        'customer_id',
        'order_number',
        'order_date',
        'total',
        'total_before_iva',
        'iva'
    ];
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class,'order_product')->withPivot('quantity', 'total');
    }
}
