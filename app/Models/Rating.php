<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rating extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'user_id',
        'rating',
        'comment'];

    public function products():BelongsTo
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
