<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public function index() : View
    {
        $category = Category::all();
        return view('category-list')->with('category',$category);
    }

    public function deleteCategory($id)
    {
        $product = Category::find($id);
        $product->delete();
        return redirect()->route('category-list')->with('success', 'Producte eliminat');
    }
    //Parte del insert
    public function insert(Request $request) {
        return view('category-insert');
    }
    public function createCategory(Request $request) {
        //Regles de les validacions
        $rules = [
            'name' => 'required|string|max:115',
            'percent' => 'required',

        ];
        //Missatges personalitzats per les validacions
        $messages = [
            'name.max' => 'El nom com ha màxim pot ser de 115 caràcters',
            'name.required' => 'El camp nom és obligatori.',

            'percent.required' => 'El camp percentatge és obligatori.',

        ];


        $request->validate($rules, $messages);

        $category = new Category();
        $category->name = $request->input('name');
        $category->percent = $request->input('percent');

        //$products->display_order = $products->id;
        $category->save();
        return redirect()->route('category-list')->with('success', 'Categoria afegit correctament');
    }

    public function confirmDelete($id)
    {
        $product = Category::find($id);
        return view('category-confirmDelete', compact('product'));
    }
}
