<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function indexOrder(Request $request) {
        $products = Product::all();
        $customers = Customer::all();
        $orders = Order::with('customer', 'products')->get();

        return view('order-list')->with([
            'customers' => $customers,
            'products' => $products,
            'orders' => $orders
        ]);
    }
    public function createOrder(){
        $customers = Customer::all();
        $products = Product::all();
        return view('order-create', compact('customers', 'products'));
    }
    public function insertOrder(Request $request) {


        $request->validate([
            'customer_id' => 'required|exists:customers,id',
            'products' => 'required|array',
            'products.*' => 'exists:products,id',
            'quantities' => 'required|array',
            'quantities.*' => 'integer|min:0',
        ]);

        $hasValidProduct = false;
        $totalAmountBeforeIva = 0;
        $ivaAmount = 0;
        $insufficientStock = [];

        foreach ($request->products as $index => $productId) {
            $quantity = $request->quantities[$index];
            if ($quantity > 0) {
                $product = Product::findOrFail($productId);
                if ($product->quantity < $quantity) {
                    $insufficientStock[] = $product->name;
                } else {
                    $hasValidProduct = true;
                    $totalAmountBeforeIva += $product->price * $quantity;
                    $ivaAmount += ($product->price * $quantity) * ($product->category->percent / 100);
                }
            }
        }

        if (!empty($insufficientStock)) {
            return redirect()->route('order-create')->with('error', 'No hi ha stock en el producte' . implode(', ', $insufficientStock));
        }

        if (!$hasValidProduct) {
            return redirect()->route('order-create')->with('error','Ha de contenir almenys un producte la comanda');
        }

        $totalAmountAfterIva = $totalAmountBeforeIva + $ivaAmount;

        $orderDate = now();
        $orderNumber = $orderDate->format('YmdHis');

        $order = Order::create([
            'customer_id' => $request->customer_id,
            'order_number' => $orderNumber,
            'order_date' => $orderDate,
            'total' => $totalAmountAfterIva,
            'total_before_iva' => $totalAmountBeforeIva,
            'iva' => $ivaAmount,
        ]);

        foreach ($request->products as $index => $productId) {
            $quantity = $request->quantities[$index];
            if ($quantity > 0) {
                $product = Product::findOrFail($productId);

                $order->products()->attach($productId, [
                    'quantity' => $quantity,
                    'unit_price' => $product->price,
                    'iva' => $product->category->percent,
                    'total' => ($product->price * $quantity) + (($product->price * $quantity) * ($product->category->percent/ 100)),
                ]);

                $product->decrement('quantity', $quantity);
            }
        }

        return redirect()->route('order-list')->with('success', 'Creada');
    }
}
