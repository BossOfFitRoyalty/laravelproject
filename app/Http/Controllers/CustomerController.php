<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CustomerController extends Controller
{
    function index() : View{
        $customers = Customer::all();
        return view('customers-list', ['customers' => $customers]);
    }

    public function delete($id) {
        $customers = Customer::find($id);
        $customers->delete();
        return redirect()->route('customers-list')->with('success', 'Producte eliminat');
    }

    public function confirmDelete($id) {
        $customers = Customer::find($id);
        return view('customers-confirmDelete', compact('customers'));
    }

    public function insert(Request $request) {
        return view('customers-insert');
    }
    public function create2(Request $request) {
        //Regles de les validacions
        $rules=[
            'name' => 'required|string|min:1',
            'address' => 'required|string|max:1000',
            'age' => 'required|integer|min:1',
            'city' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'phone'=>'required|string|min:9',
        ];
        //Missatges personalitzats per les validacions
        $messages = [
            'name.min' => 'El nom ha de tenir com a minim un caràcter',
            'address.max' => 'No es permet adresses amb més de 255 caràcters.',
            'age.min' => 'La edat ha de ser com ha mínim 1.',
            'city.max' => 'No es permet ciutats amb més de 255 caràcters.',
            'country.max' => 'No es permet paisos amb més de 255 caràcters.',
            'phone.min' => 'El número de telefon ha de contenir almenys 9 caracters',
            'name.required' => 'El camp nom és obligatori.',
            'address.required' => 'El camp addressa és obligatori.',
            'age.required' => 'El camp any és obligatori.',
            'phone.required' => 'El camp teléfon és obligatori.',
            'city.required' => 'El camp ciutat és obligatori.',
            'age.integer' => 'El preu ha de ser un nombre enter.',

        ];
        $messages = [
            'name.min' => 'El nom ha de tenir com a minim un caràcter',
            'address.max' => 'No es permet adresses amb més de 255 caràcters.',
            'age.min' => 'La edat ha de ser com ha mínim 1.',
            'city.max' => 'No es permet ciutats amb més de 255 caràcters.',
            'country.max' => 'No es permet paisos amb més de 255 caràcters.',
            'phone.min' => 'El número de telefon ha de contenir almenys 9 caracters',
            'name.required' => 'El camp nom és obligatori.',
            'address.required' => 'El camp addressa és obligatori.',
            'age.required' => 'El camp edat és obligatori.',
            'phone.required' => 'El camp teléfon és obligatori.',
            'city.required' => 'El camp ciutat és obligatori.',
            'age.integer' => 'La edat ha de ser un nombre enter.',
        ];

        $request->validate($rules, $messages);

        $customers = new Customer();
        $customers->name = $request->input('name');
        $customers->age = $request->input('age');
        $customers->address = $request->input('address');
        $customers->city = $request->input('city');
        $customers->country = $request->input('country');
        $customers->phone = $request->input('phone');
        $customers->save();

        return redirect()->route('customers-list')->with('success', 'Client afegit correctament');

    }
}
