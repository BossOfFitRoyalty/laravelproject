<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Product;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class ProductController extends Controller
{
    //Esto es llamado en la ruta
    public function index() : View
    {
        $products = Product::all();
        return view('products-list')->with('products',$products);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('products-list')->with('success', 'Producte eliminat');
    }
    //Parte del insert
    public function insert(Request $request) {
        $categories = Category::all();
        return view('products-insert',compact('categories'));
    }
    public function create(Request $request) {
        //Regles de les validacions
        $rules = [
            'name' => 'required|string|max:115',
            'description' => 'required|string|max:255',
            'price' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1',
            'category_id' => 'required',
        ];
        //Missatges personalitzats per les validacions
        $messages = [
            'name.max' => 'El nom com ha màxim pot ser de 115 caràcters',
            'description.max' => 'No es permet descripcions amb més de 255 caràcters.',
            'price.min' => 'El preu ha de ser com ha mínim 1.',
            'quantity.min' => 'La quantitat mínima ha de ser 1.',
            'name.required' => 'El camp nom és obligatori.',
            'description.required' => 'El camp descripció és obligatori.',
            'price.required' => 'El camp preu és obligatori.',
            'quantity.required' => 'El camp quantitat és obligatori.',
            'price.integer' => 'El preu ha de ser un nombre enter.',
            'quantity.integer' => 'La quantitat ha de ser un nombre enter.',
            'category_id.required'=>'Ha de ser omplert la categoria.'
        ];


        $request->validate($rules, $messages);
        $products = new Product();
        $products->name = $request->input('name');
        $products->description = $request->input('description');
        $products->price = $request->input('price');
        $products->quantity = $request->input('quantity');
        $products->category_id = $request->input('category_id');
        $products->save();
        return redirect()->route('products-list')->with('success', 'Producte afegit correctament');
    }

    public function confirmDelete($id)
    {
        $product = Product::find($id);
        return view('products-confirmDelete', compact('product'));
    }
    public function createRating(Request $request, $productId)
    {
        $rules = [
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string|max:255',
        ];

// Mensajes personalitzats per les validacions
        $messages = [
            'rating.required' => 'El camp valoració és obligatori.',
            'rating.integer' => 'La valoració ha de ser un nombre enter.',
            'rating.min' => 'La valoració ha de ser com a mínim 1.',
            'rating.max' => 'La valoració ha de ser com a màxim 5.',
            'comment.string' => 'El comentari ha de ser una cadena de text.',
            'comment.max' => 'El comentari no pot tenir més de 255 caràcters.',
        ];

        $request->validate($rules, $messages);


        Rating::create([
            'product_id' => $productId,
            'user_id' => Auth::id(),
            'rating' => $request->rating,
            'comment' => $request->comment,
        ]);

        return redirect()->route('products-list')->with('success', 'Valoració afegida correctament');
    }

    public function indexRating($id)
    {
        $product = Product::find($id);
        return view('rating-show', compact('product'));
    }
}
