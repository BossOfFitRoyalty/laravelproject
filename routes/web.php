<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

//Productes
//El index se llama en las funciones del controler
//Products sin mas
Route::get('/products', [ProductController::class, 'index'])->name('products-list');
//Productos borrar
Route::get('/products/{id}/delete', [ProductController::class, 'delete'])->name('products-delete');
//Borrar pero con aviso
Route::get('/products/{id}/delete/confirm', [ProductController::class, 'confirmDelete'])->name('products-confirmDelete');
//Ruta per inserir un producte, s'enva cap el metode insert
Route::get('/product/insert', [ProductController::class, 'insert'])->name('products-insert');
Route::post('/product/create', [ProductController::class, 'create'])->name('create');
//Ratings productes
Route::get('products/{id}/ratings', [ProductController::class, 'indexRating'])->name('rating-show');
Route::post('products/{id}/finish', [ProductController::class, 'createRating'])->name('rating-store');




//Customers
Route::get('/customers', [CustomerController::class, 'index'])->name('customers-list');
//Customers borrar
Route::get('/customers/{id}/delete', [CustomerController::class, 'delete'])->name('customers-delete');
//Borrar pero con aviso
Route::get('/customers/{id}/delete/confirm', [CustomerController::class, 'confirmDelete'])->name('customers-confirmDelete');
//Ruta per inserir un producte, s'enva cap el metode insert
Route::get('/customers/insert', [CustomerController::class, 'insert'])->name('customers-insert');
Route::post('/customers/create', [CustomerController::class, 'create2'])->name('create2');

//Orders

Route::get('/orders', [OrderController::class, 'indexOrder'])->name('order-list');
Route::get('/orders/create', [OrderController::class, 'createOrder'])->name('order-create');
Route::post('/orders/insert',[OrderController::class, 'insertOrder'])->name('order-insert');
Route::get('/orders/{id}',[OrderController::class,'show'])->name('orders-show');


//Customers
Route::get('/categories', [CategoryController::class, 'index'])->name('category-list');
//Customers borrar
Route::get('/categories/{id}/delete', [CategoryController::class, 'confirmDelete'])->name('category-delete');
//Borrar pero con aviso
Route::get('/categories/{id}/delete/confirm', [CategoryController::class, 'deleteCategory'])->name('category-confirmDelete');
//Ruta per inserir un producte, s'enva cap el metode insert
Route::get('/categories/insert', [CategoryController::class, 'insert'])->name('category-insert');
Route::post('/categories/create', [CategoryController::class, 'createCategory'])->name('category-create');




require __DIR__.'/auth.php';
