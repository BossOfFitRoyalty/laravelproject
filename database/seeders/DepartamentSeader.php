<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentSeader extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('departments')->insert(['name' => 'Personal']);
        DB::table('departments')->insert(['name' => 'Comptabilitat']);
        DB::table('departments')->insert(['name' => 'IT']);
    }
}
