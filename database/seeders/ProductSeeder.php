<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            [
                'name'=>'Small potion',
                'category_id' => 2,
                'quantity'=>50,
                'price'=>74,
                'description'=>'Description example'
            ],
            [
                'name'=>'Bastard potion',
                'category_id' => 3,
                'quantity'=>70,
                'price'=>27,
                'description'=>'Description example'
            ],
            [
                'name'=>'Mana potion',
                'category_id'=>1,
                'quantity'=>25,
                'price'=>10,
                'description'=>'Description example'
            ]
    ]);
    }
}
