<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    //Especifico el nombre

    public function run(): void
    {
        DB::table('categories')->insert([
            ['name'=>'Aventura','percent'=>50.5],
            ['name'=>'Deportes','percent'=>20.5],
            ['name'=>'Musica','percent'=>10],
        ]);
    }
}
