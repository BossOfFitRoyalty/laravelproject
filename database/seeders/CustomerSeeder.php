<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customers')->insert([
            ['name'=>'RDave', 'age'=>19, 'address'=>'c/WallStreet, 61', 'city'=>'RaveCity', 'country'=>'Wakanda','phone'=>'+34 987 654 321','created_at'=>now(), 'updated_at'=>now()]
        ]);

    }
}
