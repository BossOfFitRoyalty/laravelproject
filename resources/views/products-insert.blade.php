<x-app-layout>
    <link rel="stylesheet" type="text/css" href="{{ url('/resources/css/menu.css') }}" />
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Product') }}
        </h2>
    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    <a href="{{ route('products-list') }}" class="bg-green-500 text-blue-700 py-3 px-6 rounded-lg inline-block text-lg font-semibold hover:bg-green-600 hover:text-white">
        Retornar a Productes
    </a>




    <form class="form-horizontal" method="POST" action="{{ route('create') }}">
        @csrf
        <label for="name">Nom:</label><br>
        <input type="text" id="name" name="name" value="{{ old('name') }}" class="border rounded-lg px-3 py-1 mb-2 w-full @error('name') border-red-500 @enderror">
        @error('name')
        <span class="text-red-500 rojo">{{ $message }}</span><br>
        @enderror

        <label for="description">Descripció:</label><br>
        <input type="text" id="description" name="description" value="{{ old('description') }}" class="border rounded-lg px-3 py-1 mb-2 w-full @error('description') border-red-500 @enderror">
        @error('description')
        <span class="text-red-500 rojo">{{ $message }}</span><br>
        @enderror

        <label for="price">Preu:</label><br>
        <input type="number" id="price" name="price" value="{{ old('price') }}" class="border rounded-lg px-3 py-1 mb-2 w-full @error('price') border-red-500 @enderror">
        @error('price')
        <span class="text-red-500 rojo">{{ $message }}</span><br>
        @enderror

        <label for="quantity">Quantitat:</label><br>
        <input type="number" id="quantity" name="quantity" value="{{ old('quantity') }}" class="border rounded-lg px-3 py-1 mb-2 w-full @error('quantity') border-red-500 @enderror">
        @error('quantity')
        <span class="text-red-500 rojo">{{ $message }}</span><br>
        @enderror

        <div class="form-group">
            <label for="category_id">Categoria:</label><br>
            <select class="border rounded-lg px-3 py-1 mb-2 w-full" id="category_id" name="category_id" required>
                <option value="">Selecciona una categoria</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            @error('category_id')
            <span class="text-red-500 rojo">{{ $message }}</span><br>
            @enderror
        </div>

        <div class="flex justify-center">
            <button type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-3 px-6 rounded-lg verde">
                Afegir Producte
            </button>
        </div>

    </form>


    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .verde{
        color: #4fb64f;
    }
    .rojo{
        color: red;
    }
</style>
