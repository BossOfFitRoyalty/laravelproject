<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Order List') }}
        </h2>

    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    @if(session('success'))
        <div class="bg-green-200 text-green-800 p-4 mb-4">
            {{ session('success') }}
        </div>
    @endif

    <div class="container mx-auto mt-6 flex justify-center">
        <table class="min-w-full bg-white border border-gray-200 shadow-sm rounded-lg overflow-hidden">
            <thead class="bg-gray-200">
            <tr>
                <th class="px-4 py-2">Comanda Id</th>
                <th class="px-4 py-2">Client</th>
                <th class="px-4 py-2">Productes</th>
                <th class="px-4 py-2">Abans IVA</th>
                <th class="px-4 py-2">IVA</th>
                <th class="px-4 py-2">Total</th>
                <th class="px-4 py-2">Data comanda</th>
            </tr>
            </thead>
            <tbody class="divide-y divide-gray-200">
            @foreach ($orders as $order)
                <tr>
                    <td class="px-4 py-2">{{ $order->order_number }}</td>
                    <td class="px-4 py-2">{{ $order->customer->name }}</td>
                    <td class="px-4 py-2">
                        <ul>
                            @foreach ($order->products as $product)
                                <li>{{ $product->name }} ({{ $product->pivot->quantity }})</li>
                            @endforeach
                        </ul>
                    </td>
                    <td class="px-4 py-2">${{ $order->total_before_iva }}</td>
                    <td class="px-4 py-2">${{ $order->iva }}</td>
                    <td class="px-4 py-2">${{ $order->total }}</td>
                    <td class="px-4 py-2">{{ $order->order_date }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="flex justify-center mt-4">
        <a href="{{ route('order-create') }}" class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mr-2 verde">Crear Comanda</a>
        <a href="{{ route('customers-insert') }}" class="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded ml-2 azul">Afegir clients</a>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mt-6">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>

</x-app-layout>

<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .verde{
        color: green;
    }
    .azul{
        color: blue;
    }
</style>


