<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Confirm Delete Product') }}
        </h2>
    </x-slot>

    <div class="max-w-2xl mx-auto mt-6 p-6 bg-white shadow-md rounded-lg">

        <p class="text-gray-700 mb-4">Estàs segur que vols esborrar aquest producte?</p>

        <div class="flex space-x-4">
            <a href="{{ route('products-delete', $product->id) }}" class="btn-delete">Esborrar</a>
            <a href="{{ route('products-list') }}" class="btn-cancel">Cancelar</a>
        </div>



        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mt-6">
                    <div class="p-6 text-gray-900">
                        {{ __("You're logged in!") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<style>
    .btn-delete {
        background-color: #34D399; /* Verde */
        color: white;
        padding: 0.5rem 1rem;
        border-radius: 0.375rem; /* 6px */
        text-decoration: none;
    }

    .btn-delete:hover {
        background-color: #10B981; /* Verde más oscuro al pasar el mouse */
    }

    .btn-cancel {
        background-color: #EF4444; /* Rojo */
        color: white;
        padding: 0.5rem 1rem;
        border-radius: 0.375rem; /* 6px */
        text-decoration: none;
    }

    .btn-cancel:hover {
        background-color: #F87171; /* Rojo más oscuro al pasar el mouse */
    }

</style>

