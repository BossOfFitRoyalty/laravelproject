<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Order List') }}
        </h2>
    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    @if(session('success'))
        <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4" role="alert">
            <p class="font-bold">Success!</p>
            <p>{{ session('success') }}</p>
        </div>
    @endif

    @if(session('error'))
        <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4" role="alert">

            <p class="rojo">{{ session('error') }}</p>
        </div>
    @endif

    <div class="container mx-auto mt-6">
        <form action="{{ route('order-insert') }}" method="POST">
            @csrf
            <div class="mb-4">
                <label for="customer_id" class="block text-gray-700 text-sm font-bold mb-2">Client:</label>
                <select name="customer_id" id="customer_id" class="form-select block w-full p-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:border-blue-500 focus:ring focus:ring-blue-200 focus:ring-opacity-50" required>
                    <option value="">Select Client</option>
                    @foreach ($customers as $customer)
                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="table-responsive">
                <table class="table-auto w-full border-collapse border border-gray-300">
                    <thead>
                    <tr class="bg-gray-200">
                        <th class="px-4 py-2">Product</th>
                        <th class="px-4 py-2">Category</th>
                        <th class="px-4 py-2">Unit Price</th>
                        <th class="px-4 py-2">IVA (%)</th>
                        <th class="px-4 py-2">Quantity</th>
                        <th class="px-4 py-2">Total Price Before IVA</th>
                        <th class="px-4 py-2">Total IVA Amount</th>
                        <th class="px-4 py-2">Total Price After IVA</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td class="border px-4 py-2">{{ $product->name }}</td>
                            <td class="border px-4 py-2">{{ $product->category->name }}</td>
                            <td class="border px-4 py-2">€{{ $product->price }}</td>
                            <td class="border px-4 py-2">{{ $product->category->percent }}%</td>
                            <td class="border px-4 py-2">
                                <input type="number" name="quantities[]" class="form-input block w-full p-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:border-blue-500 focus:ring focus:ring-blue-200 focus:ring-opacity-50" value="0" min="0" required>
                                <input type="hidden" name="products[]" value="{{ $product->id }}">
                                <input type="hidden" name="prices[]" value="{{ $product->price }}">
                            </td>
                            <td class="border px-4 py-2">€<span class="total-price-before-iva">0.00</span></td>
                            <td class="border px-4 py-2">€<span class="total-iva-amount">0.00</span></td>
                            <td class="border px-4 py-2">€<span class="total-price-after-iva">0.00</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="flex justify-center items-center h-screen">
                <div>
                    <button type="submit" class="bg-blue-500 hover:bg-blue-600 text-white font-bold text-xl py-4 px-8 rounded verde">Crear Comanda</button>
                </div>
            </div>

        </form>
    </div>


    <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900">
                        {{ __("You're logged in!") }}
                    </div>
                </div>
            </div>
        </div>
</x-app-layout>


<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .verde{
        color: green;
    }
    .rojo{
        color: red;
    }
</style>

<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        document.querySelectorAll('input[name="quantities[]"]').forEach(input => {
            input.addEventListener('input', function() {
                const row = this.closest('tr');
                const unitPrice = parseFloat(row.querySelector('input[name="prices[]"]').value);
                const quantity = parseInt(this.value);
                const ivaPercentage = parseFloat(row.querySelector('td:nth-child(4)').textContent.replace('%', '')) / 100;
                const totalPriceBeforeIva = unitPrice * quantity;
                const totalIvaAmount = totalPriceBeforeIva * ivaPercentage;
                const totalPriceAfterIva = totalPriceBeforeIva + totalIvaAmount;

                row.querySelector('.total-price-before-iva').textContent = totalPriceBeforeIva.toFixed(2);
                row.querySelector('.total-iva-amount').textContent = totalIvaAmount.toFixed(2);
                row.querySelector('.total-price-after-iva').textContent = totalPriceAfterIva.toFixed(2);
            });
        });
    });
</script>
