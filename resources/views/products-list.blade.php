<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product List') }}
        </h2>
    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    @if(session('success'))
        <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4" role="alert">
            <span class="block sm:inline valoracion">{{ session('success') }}</span>
        </div>
    @endif

    <div class="container mx-auto">
        @foreach($products as $product)
            <div class="bg-white rounded-lg shadow-lg mb-4">
                <div class="p-4 flex justify-between items-center">
                    <div class="w-1/4">
                        <h3 class="text-xl font-bold">{{ $product->name }}</h3>
                        <p class="text-gray-600">{{ $product->description }}</p>
                    </div>
                    <div class="w-1/4">
                        <p><strong>Cantidad:</strong> {{ $product->quantity }}</p>
                        <p><strong>Precio:</strong> ${{ $product->price }}</p>
                        <p><strong>Categoría:</strong> {{ $product->category->name }}</p>
                    </div>
                    <div class="w-1/4">
                        <a href="{{ route('products-confirmDelete', $product->id) }}" class="text-red-600 hover:text-red-900">Borrar</a>
                        <a href="{{ route('rating-show',$product->id) }}" class="ml-2 text-blue-500 hover:text-blue-800 valoracion">Añadir valoración</a>
                    </div>
                </div>

                <!-- Listado de valoraciones -->
                <div class="bg-white p-4">
                    <h4 class="text-lg font-bold mb-2">Valoraciones</h4>
                    @forelse($product->ratings as $rating)
                        <div class="border-t border-gray-200 pt-4">
                            <div class="flex justify-between items-center mb-2">
                                <div>
                                    <p class="font-semibold">Usuario: {{ $rating->user->name }}</p>
                                    <p class="text-sm text-gray-600">{{ $rating->created_at->format('d M Y') }}</p>
                                </div>
                                <div class="flex items-center">
                                    <p class="text-yellow-500">{{ str_repeat('★', $rating->rating) }}</p>
                                    <p class="text-gray-600 ml-2">{{ $rating->rating }}</p>
                                </div>
                            </div>
                            <p class="text-sm">{{ $rating->comment }}</p>
                        </div>
                    @empty
                        <p class="text-sm text-gray-500 py-2">No hay valoraciones para este producto.</p>
                    @endforelse
                </div>
            </div>
        @endforeach
    </div>



    <!-- Enllaço cap a la vista del insert per després allí realitzar la funció-->
    <div class="flex justify-center">
        <a href="{{ route('products-insert') }}" class="text-lg bg-green-500 hover:bg-green-600 valoracion font-bold py-2 px-4 rounded inline-flex items-center">
            Afegir producte
        </a>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
</style>

<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .valoracion{
        color: #4fb64f;
    }
</style>

