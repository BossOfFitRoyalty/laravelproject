<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Category List') }}
        </h2>

    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    @if(session('success'))
        <div class="verde bg-green-500 text-white p-4 rounded mb-4">
            {{ session('success') }}
        </div>
    @endif
    <div class="container mx-auto flex justify-center">

            <table class="min-w-full bg-white">
                <thead>
                <tr class="bg-gray-200 text-gray-700 uppercase text-lg leading-normal">
                    <th class="py-3 px-6 text-left">Nombre</th>
                    <th class="py-3 px-6 text-left">Porcentaje</th>
                    <th class="py-3 px-6 text-center">Acciones</th>
                </tr>
                </thead>
                <tbody class="text-gray-600 text-lg font-light">
                @foreach($category as $cat)
                    <tr class="border-b border-gray-200 hover:bg-gray-100">
                        <td class="py-4 px-6 text-left whitespace-nowrap">{{ $cat->name }}</td>
                        <td class="py-4 px-6 text-left whitespace-nowrap">{{ $cat->percent }}</td>
                        <td class="py-4 px-6 text-center">
                            <a href="{{ route('category-confirmDelete', $cat->id) }}" class="text-red-500 hover:text-red-700 underline rojo">Borrar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

    </div>


    <div class="container mx-auto flex justify-center items-center h-screen">
        <a href="{{ route('category-insert') }}" class="verde bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-8 rounded-lg text-3xl">
            Afegir categories
        </a>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .verde{
        color: green;
    }
    .rojo{
        color: red;
    }
</style>

