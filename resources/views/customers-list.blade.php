<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Customer List') }}
        </h2>
    </x-slot>
    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>
    @if(session('success'))
        <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4">
            {{ session('success') }}
        </div>
    @endif

    <div class="container mx-auto mt-6 flex justify-center">

            <table class="min-w-full bg-white border border-gray-200 shadow-sm rounded-lg overflow-hidden">
                <thead class="bg-gray-200">
                <tr>
                    <th class="px-4 py-2">Nom</th>
                    <th class="px-4 py-2">Edat</th>
                    <th class="px-4 py-2">Adreça</th>
                    <th class="px-4 py-2">Ciutat</th>
                    <th class="px-4 py-2">País</th>
                    <th class="px-4 py-2">Telèfon</th>
                    <th class="px-4 py-2">Accions</th>
                </tr>
                </thead>
                <tbody class="divide-y divide-gray-200">
                @foreach($customers as $customer)
                    <tr>
                        <td class="px-4 py-2">{{ $customer->name }}</td>
                        <td class="px-4 py-2">{{ $customer->age }}</td>
                        <td class="px-4 py-2">{{ $customer->address }}</td>
                        <td class="px-4 py-2">{{ $customer->city }}</td>
                        <td class="px-4 py-2">{{ $customer->country }}</td>
                        <td class="px-4 py-2">{{ $customer->phone }}</td>
                        <td class="px-4 py-2">
                            <a href="{{ route('customers-confirmDelete', $customer->id) }}" class="bg-red-500 hover:bg-red-600 text-white font-bold py-1 px-3 rounded rojo">Borrar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

    </div>


    <!-- Enllaço cap a la vista del insert per després allí realitzar la funció-->
    <div class="flex justify-center items-center h-screen ">
        <a href="{{ route('customers-insert') }}" class=" verde bg-green-500 hover:bg-green-600 text-white font-bold py-4 px-8 rounded text-3xl">
            Afegir clients
        </a>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
    .rojo{
        color: red;
    }
    .verde{
        color: green;!important;
    }
</style>

