<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product Details') }}
        </h2>
    </x-slot>

    <nav class="flex flex-wrap justify-center mt-4">
        <a href="{{ url('/') }}" class="nav-link">Welcome</a>
        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
        <a href="{{ route('customers-list') }}" class="nav-link">Clients</a>
        <a href="{{ route('products-list') }}" class="nav-link">Products</a>
        <a href="{{ route('order-list') }}" class="nav-link">Orders</a>
        <a href="{{ route('category-list') }}" class="nav-link">Categories</a>
    </nav>

    <div class="container mx-auto">
        <!-- Información del producto -->
        <div class="bg-white p-6 rounded-lg shadow-lg">
            <h3 class="text-2xl font-bold mb-4">{{ $product->name }}</h3>
            <p class="mb-4">{{ $product->description }}</p>
            <p class="mb-4">Precio: ${{ $product->price }}</p>

            <!-- Formulario para agregar valoración -->
            @auth
                <form action="{{ route('rating-store', $product->id) }}" method="POST" class="space-y-4">
                    @csrf
                    <div class="flex flex-col">
                        <label for="rating" class="text-sm font-medium text-gray-700">Valoración</label>
                        <select name="rating" id="rating" class="mt-1 form-select block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" required>
                            <option value="">Seleccionar</option>
                            @for($i = 1; $i <= 5; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="flex flex-col">
                        <label for="comment" class="text-sm font-medium text-gray-700">Comentario</label>
                        <textarea name="comment" id="comment" rows="3" class="mt-1 form-textarea block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"></textarea>
                    </div>
                    <button type="submit" class="w-full inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enviar
                    </button>
                </form>
            @else
                <p class="mt-4">Por favor <a href="{{ route('login') }}" class="text-blue-500">inicia sesión</a> para dejar una valoración.</p>
            @endauth
        </div>
    </div>

</x-app-layout>

<style>
    .nav-link {
        padding: 8px 16px;
        margin: 4px;
        background-color: #ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        text-decoration: none;
        color: #000000;
        transition: all 0.3s ease;
    }

    .nav-link:hover {
        background-color: #f0f0f0;
        border-color: #e0e0e0;
        color: #000000;
    }

    .nav-link:focus {
        outline: none;
        box-shadow: 0 0 0 2px #FF2D20; /* Cambia el color del borde al enfocar */
    }

    .nav-link:focus-visible {
        box-shadow: 0 0 0 2px #FF2D20; /* Resalta el borde al enfocar con teclado */
    }
</style>
